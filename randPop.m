function Pop = randPop(D, Np, ub, lb) %ub lb can be vectors of dimension D
   randMatrix = rand(Np,D); %Np rows, Np food sources. D columns, D dimension
   Pop = lb+randMatrix.*(ub-lb);
end