function pm = probabilityMatrix(fitness) %ub lb can be vectors of dimension D
    pm = zeros(size(fitness));
    maxFitness = max(fitness);
    for y=1:size(fitness)
      pm(y) = 0.9*(fitness(y)/maxFitness)+0.1;
    end
end