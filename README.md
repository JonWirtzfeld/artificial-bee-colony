# Artificial Bee Colony

Simple MATLAB implementation of an Artificial Bee Colony algorithm. Its designed to find a value of Q to minimize energy cost in an LQR control problem for an inverted pendulum system,
though the fitness function can easily be modified to optimize for other search problems. It was designed for a Modern Controls class labratory project, although it was eventuall scrapped because 
there were specifications other than energy cost that our control system needed to be solved for that could not efficiently be considered by this algorithm. The use of this algorithm to solve LQR was inspired by [this](https://ieeexplore.ieee.org/document/6681769) paper. 
The specific implimentation was based off the information in [this very helpful video](https://www.youtube.com/watch?v=U9ah51wjvgo). 
