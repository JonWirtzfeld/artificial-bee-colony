function fitFS = fitnessFoodSource(fs, cartic, angleic) 
% Parameters
    Rm = 2.6; % 12
    kt = 7.68*10^-3; % 12
    km = 7.68*10^-3; % 12
    nm = 0.69; % 0.69 5
    Jm = 3.9 *10^-7; %10
    kg = 3.71;
    ng = 0.9; % 0.9 10
    Mc = 0.38;
    Mw = 0.37;
    Beq = 5.4;
    rmp = 6.35*10^-3;
    Mp = 0.23;
    Lp = 0.3302;
    Jp = 7.88*10^-3;
    Bp = 0.0024;
    g = 9.81;
    M = Mc+Mw+Mp;
    Jeq = (M+(ng*Jm*kg^2)/(rmp^2));

    %Lab 4
    % Components of Matrices
    JT=Mp*Jp+Jeq*Jp+Mp*Lp^2*Jeq;
    a32=(Lp^2*Mp^2*g)/JT;
    a33=-((Jp+Lp^2*Mp)*(((ng*kg^2*km*kt)/(Rm*rmp^2))+Beq))/JT;
    a34=-(Lp*Mp*Bp)/JT;
    a42=(Mp*Lp*g*(Mp+Jeq))/JT;
    a43=-((Mp*Lp)*(((ng*kg^2*km*kt)/(Rm*rmp^2))+Beq))/JT;
    a44=-((Mp+Jeq)*Bp)/JT;
    b31=((nm*ng*kg*kt)*(Jp+Lp^2*Mp))/(Rm*rmp*JT);
    b41=(Lp*Mp*nm*ng*kg*kt)/(Rm*rmp*JT);
    % Matrices
    A=[0 0 1 0; 0 0 0 1; 0 a32 a33 a34; 0 a42 a43 a44];
    eig(A);
    B1=[0 0 b31 b41]';

    Q=[fs(1) 0 0 0;0 fs(2) 0 0;0 0 fs(3) 0;0 0 0 fs(4)]; %place optimized q here
    R=1; %This is for code-testing purpose, need to optimize R
    N=[0;0;0;0];
    [~,S,~]=lqr(A,(-1*B1),Q,R,N); %Km1 is K method 1, S is our M (from class), E is eigenvalues of A-B1*Km1 (Multiplied -1*B1 so we have A+B1*K1)
    xic = [cartic; angleic; 0; 0];
    JCost = transpose(xic)*S*xic;
    fitFS = 1/(1+JCost);

end