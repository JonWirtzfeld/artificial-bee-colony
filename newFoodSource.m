%newFoodSource takes in a population and an row index (food source)
%it returns a modified food source
%Uses a randomly selected partner to modify
function xnew = newFoodSource(pop, i, ub, lb) %ub lb can be vectors of dimension D
    [Np, D] = size(pop);
    k = 0;
    while (k==0 || k==i)
        k = round(1 + rand*(Np-1)); %index of partner
    end
    dim = round(1 + rand*(D-1)); %random value within the food source
                                 %up for modification
    xnew = pop(i, 1:end);
    xnew(dim) = pop(i,dim) + (2*rand(1)-1)*(pop(i,dim)-pop(k,dim));
    if xnew(dim) < lb
        xnew(dim) = lb;
    end
    if xnew(dim) > ub
        xnew(dim) = ub;
    end
end