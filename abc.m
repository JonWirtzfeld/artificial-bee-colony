%abc - artificial bee colony algorith 
clear all;
s = input('Input Swarm size: ');
T = input('Input number of cycles: ');
lb = input('Input Q lower-bound: ');
ub = input('Input Q upper-bound: ');
cartic = input('Input cart initial conditions: ');
angleic = input('Input pendulum initial conditions: ');

D = 4;
Np = s/2;
limit = Np*D;
population = randPop(D, Np, ub, lb);
trial = zeros(Np,1);
bestSoln = 0;

for t=1:T 
    %Employed Bee Phase
    for i=1:Np %Every foodsource gets exploited once
        %pick partner
        partner = newFoodSource(population, i, ub, lb);
        %fitness of current food source
        currentfit = fitnessFoodSource(population(i,1:end), cartic, angleic); 
        %fitness of new food source
        newfit = fitnessFoodSource(partner, cartic, angleic);                
        if newfit > currentfit
            population(i,1:end) = partner; 
            trial(i) = 0;
        else 
            trial(i) = trial(i) + 1;
        end
    end
    %Onlooker Bee Phase
    %determine fitness of all food sources
    fitMatrix = fitnessMatrix(population, cartic, angleic);
    %determine probability of each food source
    probMatrix = probabilityMatrix(fitMatrix);
    %vector of bees
    OB = zeros(Np,1);
    m=1;
    n=1;
    staticPopulation = population;
    while m<=Np
        r = rand;
        if r<probMatrix(n)
            nfs = newFoodSource(staticPopulation,n,ub,lb);
            fits = fitnessFoodSource(nfs, cartic, angleic);
            if fits>fitMatrix(n)
                population(m,1:end) = nfs;
                fitMatrix(n)= fits;
                trial(n) = 0;
            else 
                trial(n) = trial(n) + 1;
            end
            m = m+1;
        end 
        n = n + 1;
        %list may be gone through several times
        %before each bee has a new value
        if n > Np
            n = 1;
        end 
    end %end while, end OBP
    %Scout Bee Phase
    %memorize best soln
    [bestIterativeSoln, bisIndex] = max(fitMatrix);
    
    if bestIterativeSoln > bestSoln
        bestSoln = bestIterativeSoln;
        bestQ = population(bisIndex, 1:end); 
    end
    plot(t,bestSoln,'.')
    hold on
    %check if scout phase will occure
    if max(trial) > limit
        [~,maxIndex] = max(trial);
        nfs = lb+rand*(ub-lb);
        for i=1:D
            population(maxIndex, i) = lb+rand*(ub-lb);
        end
        fitMatrix(maxIndex) = fitnessFoodSource(population(maxIndex,1:end), cartic, angleic);
        trial(maxIndex) = 0;
    end
end
disp("The best Q found: ")
disp(bestQ)